export default {
  throwMethodNotAllowed(key, res) {
    res.status(401).json({
      [key]: {
        error: 'You are not allowed to access this resource.'
      }
    })
  },

  throwUnprocessableEntity(key, errors, res) {
    res.status(422).json({
      [key]: {
        errors: errors.mapped()
      }
    })
  }
}
