import Serializer from '@common/serializer'

export default {
  ...Serializer,

  signup(user) {
    return { token: user.generateToken() }
  },

  signin(user) {
    return { token: user.generateToken() }
  }
}
