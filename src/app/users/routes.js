import controller from './controller'

export function setup(router) {
  router.post('/signup', controller.signup).post('/signin', controller.signin)
}
