import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import morgan from 'morgan'

import { setupRoutes, connectDatabase, config, setupAuth } from './config'

export function setup() {
  const app = express()
  const { port, host } = config

  app.set('view engine', 'ejs')
  app.set('views', path.join(__dirname, 'app/views'))
  app.set('layout', 'shared/layout')
  app.use(morgan('tiny'))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  setupAuth(app)
  setupRoutes(app)

  connectDatabase().then(() =>
    app.listen(port, () =>
      console.log(`App listening on http://${host}:${port}`)
    )
  )
}
